<?php

/*https://www.anerbarrena.com/php-array-tipos-ejemplos-3876/
 * Carlos Servín
 * Define array types
 * 24/07/18
 * 
 * */
 
 function arraynumerico(){
 	$variable = array($valor1, $valor2, $valor2);
	return $variable;
 }
 


 function arraytype1(){
 	// La sintaxis es antigua, esta es probable que la marque como error
 	//Este tipo de sintaxis esta generando errores en algunos IDE como APTANA Y BRACKETS al momento de colapsar llaves y ocultar código
 	return ['error'=>'Tipo de arreglo 1'];
 }
 
 function arraytype2(){
 	// sintaxis actual produce el mismo resultado que la sintaxis 1
 	return array('error'=>'Tipo de arreglo 2');
 }
 
 function arrayasociativo(){
 	$array=array();
 	//ejemplo para acceder al primer elemento
	$array[0];
	//ejemplo para acceder al segundo elemento
	$array[1];
	//ejemplo para acceder al tercero elemento
	$array[2];
	//ejemplo para acceder al cuarto elemento
	$array[3];
	//ejemplo para acceder al quinto elemento
	$array[4];
	//ejemplo para acceder al sexto elemento
	$array[5];
	
	return $array;
 }
 
 $result=arraytype1();
 $result2=arraytype2();
 $result3=arrayasociativo();
 
 
 print_r($result);
 
 echo "<br><br><br>";
 
 print_r($result2);

 echo "<br><br><br>"; 

print_r($result3);
//Arrays Multidimiensionales

//Array simple o númerico indexado


//Array Asociativo



?>