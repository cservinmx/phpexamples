<?php
//https://www.opencodez.com/php/build-a-web-service-using-php.htm
require('lib/nusoap.php');

// Try to get method and parameters from query string
$method = isset($_GET["method"]) ? $_GET["method"] : "add";
$param1 = isset($_GET["param1"]) ? $_GET["param1"] : "4";
$param2 = isset($_GET["param2"]) ? $_GET["param2"] : "4";

// Create the client instance
$client = new nusoap_client('http://localhost/soap-server.php?wsdl', true);

$err = $client->getError();
if ($err) {
	echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
}

$param = array("param1" => $param1 ,"param2" => $param2);

$result = $client->call($method, array('parameters' => $param));

// Check for a fault
if ($client->fault) {
	echo '<h2>Fault</h2><pre>';
	print_r($result);
	echo '</pre>';
} else {
	// Check for errors
	$err = $client->getError();
	if ($err) {
		// Display the error
		echo '<h2>Error</h2><pre>' . $err . '</pre>';
	} else {
		// Display the result
		echo '<h2>Result</h2><pre>';
		print_r($result["return"]);
		echo '</pre>';
	}
}
echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->debug_str, ENT_QUOTES) . '</pre>';
?>