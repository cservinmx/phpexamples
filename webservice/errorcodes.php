<?php
    require_once "lib/nusoap.php";
      
        function getProd($categoria) {
        if ($categoria == "error") {
            return join(",", array(
                'El ruc del seller deber contener solo dígitos',
			  	'El ruc del seller es null o vacío',	
			  	'Error de cantidad de dígitos del ruc de seller (debe ser 11)',
				));
        }
        else {
                return "No hay errores de esta categoria";
        }
    }
      
    $server = new soap_server();
    $server->configureWSDL("err", "urn:err");
      
    $server->register("getProd",
        array("categoria" => "xsd:string"),
        array("return" => "xsd:string"),
        "urn:err",
        "urn:err#getProd",
        "rpc",
        "encoded",
        "Nos da una lista de errores de cada categoría");
      
    //$server->service($HTTP_RAW_POST_DATA);
    
    
    function createUpsShipment($create_type,$client_id,$shipment,$reqtype="beta",$shipper_id = NULL,$service_override=NULL){
	
	global $db;
	
	$db_trackinglog = "national_ups_tracking_log";
	
	if($reqtype=="beta") $url = "https://wwwcie.ups.com/rest/Ship"; // test
	else if($reqtype=="prod") $url = "https://onlinetools.ups.com/rest/Ship"; // prod
	
	if($create_type=="ecomm"){
		$clientData = $db->getRowAssoc("select * from national_clients where client_id = '".$client_id."'");
		$orderData = $db->getRowAssoc("select * from ".$clientData['db_table']." where ship_id = '".$shipment."'");
	} else {
		$clientData = json_decode($db->getVal("select ups_params from stores where store_id = ".$client_id),true);
		$storeaccount = $clientData['accounts'][0]['number'];
		$orderData = $db->getRowAssoc("select * from ".$_SESSION['LOGIN_ADMIN']['iso']."_shipping where ship_id = ".$shipment);
	}
	
	if($clientData && $orderData){


	define('SHIP_LABEL', 'shiplabel-'.$client_id.'-'.$orderData['ship_id'].'-'.date('YmdHis').'.gif'); 
		
	$service = $orderData['shipping_service'];
	$client_service =  $db->getRow("select courier_account,extra,client_service_name from national_clients_services where servicio_code2 = '".$service."' and client_id = ".$client_id." and status = 1");
		
	$account = $client_service['courier_account'];
		
	if(!$shipper_id) $shipper_id = $orderData['shipper_id'];

	$recipient = json_decode($orderData['recipient'],true);
	$pkg = json_decode($orderData['shipment_detail'],true);
	if($pkg['courier']) $pkg = $pkg['courier']; //form creation
		
		if($shipper_id){
		
			if($create_type=="ecomm")
				$aryShipper = $db->getRow("select * from national_shippers where shipper_id = ".$shipper_id);
			else if($create_type=="ship")
				$aryShipper = $db->getRow("select * from ".$_SESSION['LOGIN_ADMIN']['iso']."_shippers where shipper_id = ".$shipper_id);
					
			$account = ($aryShipper['courier_account_override'])?
				$account = $aryShipper['courier_account_override']:
				$account = $account;
									
			$shipper  = '{
						"Name": "'.$aryShipper["company_name"].'",
						"AttentionName": "'.$aryShipper["person_name"].'",
						"Phone": 
							{
							"Number":"'.$aryShipper["phone"].'"
							},
						"ShipperNumber": "32V175",
						"Address": 
							{
							"AddressLine":"'.$aryShipper["address1"].'",
							"AddressLine":"'.$aryShipper["address2"].'",
							"City":"'.$aryShipper["city"].'",
							"StateProvinceCode":"'.$aryShipper["state"].'",
							"PostalCode":"'.$aryShipper["postal_code"].'",
							"CountryCode":"'.$aryShipper["country_code"].'"
							}
						}';
			
			$shipfrom  = '{
						"Name": "'.$aryShipper["company_name"].'",
						"AttentionName": "'.$aryShipper["person_name"].'",
						"Phone": 
							{
							"Number":"'.$aryShipper["phone"].'"
							},
						"Address": 
							{
							"AddressLine":[	
								"'.$aryShipper["address1"].'",
								"'.$aryShipper["address2"].'"
							],
							"City":"'.$aryShipper["city"].'",
							"StateProvinceCode":"'.$aryShipper["state"].'",
							"PostalCode":"'.$aryShipper["postal_code"].'",
							"CountryCode":"'.$aryShipper["country_code"].'"
							}
						}';
									
		}
		
		else if($clientData['shipper']){
			
			$aryShipper = json_decode($clientData['shipper'],true);
			
			$account = ($aryShipper['courier_account_override'])?
				$account = $aryShipper['courier_account_override']:
				$account = $account;
			
			$shipper  = '{
						"Name": "'.$aryShipper["Contact"]["CompanyName"].'",
						"AttentionName": "'.$aryShipper["Contact"]["PersonName"].'",
						"Phone": 
							{
							"Number":"'.$aryShipper["Contact"]["PhoneNumber"].'"
							},
						"ShipperNumber": "32V175",
						"Address": 
							{
							"AddressLine": [
								"'.$aryShipper["Address"]["StreetLines"][0].'",
								"'.$aryShipper["Address"]["StreetLines"][1].'",
								"'.$aryShipper["Address"]["StreetLines"][2].'"
							],
							"City":"'.$aryShipper["Address"]["City"].'",
							"StateProvinceCode":"'.$aryShipper["Address"]["StateOrProvinceCode"].'",
							"PostalCode":"'.$aryShipper["Address"]["PostalCode"].'",
							"CountryCode":"'.$aryShipper["Address"]["CountryCode"].'"
							}
						}';
			
			$shipfrom = $shipper;
						
		} 
		else {
			
			$shipper = '{
						"Name": "Mail Boxes Etc. Corporativo",
						"AttentionName": "Mail Room MBE",
						"Phone": 
							{
							"Number":"55500041919"
							},
						"ShipperNumber": "32V175",
						"Address": 
							{
							"AddressLine": [ 
								"AV Ejercito Nacional 253-A",
								"Col Anahuac",
								"Torre YMCA 4to Piso"
							],
							"City":"CDMX",
							"StateProvinceCode":"DF",
							"PostalCode":"11320",
							"CountryCode":"MX"
							}
						}';
			
			$shipfrom = $shipper;
		}
		
		$dimunit = ($pkg['package_dim_unit']=="CM")? "centimeters": "inches";
		$weightunit = ($pkg['package_weight_unit']=="KG")? "kilograms": "pounds";
		
		if($pkg['MPSData']&&count($pkg['MPSData'])>1){
			$packages = '[';
			for($i=0;$i<count($pkg['MPSData']);$i++){
				$ipkg = '{
						"Description":"Paquete '.$pkg["MPSData"][$i]["Ref"].'",
						"Packaging":
							{"Code":"02","Description":"Description"},
						"Dimensions": {
							"UnitOfMeasurement":
								{"Code":"CM","Description":"Centimeters"},
							"Length":"'.$pkg["MPSData"][$i]["Length"].'",
							"Width":"'.$pkg["MPSData"][$i]["Width"].'",
							"Height":"'.$pkg["MPSData"][$i]["Height"].'"
							},
						"PackageWeight": {
							"UnitOfMeasurement":
								{"Code": "KGS"},
							"Weight":"'.$pkg["MPSData"][$i]["Weight"].'"
						}
						},';
				
				$packages .= $ipkg;
			}
			$packages = substr($packages,0,-1);
			$packages .= "]";
		} else {
			
			$packages = '{
						"Description":"'.$pkg["Description"].'",
						"Packaging": {
							"Code":"02",
							"Description":"Description"
							},
						"Dimensions" : {
							"UnitOfMeasurement":
								{"Code":"CM","Description":"Centimeters"},
							"Length":"'.$pkg["Length"].'",
							"Width":"'.$pkg["Width"].'",
							"Height":"'.$pkg["Height"].'"
							},
						"PackageWeight": {
							"UnitOfMeasurement":
								{"Code": "KGS"},
							"Weight":"'.$pkg["Weight"].'"
						}
						}';
			
		}
		$cnt_pkgs = (count($pkg['MPSData'])) ? count($pkg['MPSData']) : "1";
		
		
		// valor declarado
		
		if($orderData['assessed']>0){
			
			
			$decval = ',
						"PackageServiceOptions":{
							"DeclaredValue":{
								"CurrencyCode":"MXN",
								"MonetaryValue":"'.str_replace(",","",$orderData['assessed']).'"
							}
						}';
			
			$packages = $packages.$decval;
			
			//echo $packages; break;
			
		}
		
		
		// final request
		
		$request = '{
					"UPSSecurity": {
						"UsernameToken": {
							"Username":"XML-MB","Password":"MBE100Corpo"
							},						
						"ServiceAccessToken": {
							"AccessLicenseNumber":"EC964A2DC691BEF3"
							}
						},
						"ShipmentRequest": {
							"Request": {
								"RequestOption":"validate",
								"TransactionReference": {
									"CustomerContext" : "ID '.$client_id.'-'.$shipment.'"
								}
							},
							"Shipment": {
								"Description":"'.$pkg["Description"].'",
								"NumOfPiecesInShipment":"'.$cnt_pkgs.'",
								"Shipper": '.$shipper.',
								"ShipTo": {
									"Name": "'.$recipient["Contact"]["PersonName"].'",
									"AttentionName": "'.$recipient["Contact"]["CompanyName"].'",
									"Phone": 
										{
										"Number":"'.$recipient["Contact"]["PhoneNumber"].'"
										},
									"Address": 
										{
										"AddressLine": [
											"'.$recipient["Address"]["StreetLines"][0].'",
											"'.$recipient["Address"]["StreetLines"][1].'",
											"'.$recipient["Address"]["StreetLines"][2].'"
										],
										"City":"'.$recipient["Address"]["City"].'",
										"StateProvinceCode":"'.$recipient["Address"]["StateOrProvinceCode"].'",
										"PostalCode":"'.$recipient["Address"]["PostalCode"].'",
										"CountryCode":"MX"
										}
								},
								"ShipFrom" : '.$shipfrom.',
								"PaymentInformation": {
									"ShipmentCharge": {
										"Type": "01",
										"BillThirdParty": {
											"AccountNumber": "'.$account.'",
											"Address": {
												"PostalCode": "'.$client_service["extra"].'",
												"CountryCode": "MX"
											}
										}
									}
								},
								"Service": {
									"Code": "'.$orderData["shipping_service"].'",
									"Description": "'.$client_service["client_service_name"].'"
								},
								"Package": '.$packages.'
							},
							"LabelSpecification": {
								"LabelImageFormat": {
									"Code": "GIF",
									"Description": "GIF"
								},
								"HTTPUserAgent": "Mozilla\/4.5"
							}
						}
					
				}';
		
				
	}
	
	$request = str_replace("\t","",$request);
	
	//if($_SESSION['LOGIN_ADMIN']['user_id']==5) echo $request;
	
	//echo $request;

	/////// CURL IT

	$response = doCurl($url,$request);
	
	$response_obj = json_decode($response);
	
	//if($_SESSION['LOGIN_ADMIN']['user_id']==5) print_r($response_obj);
	
	if($response_obj->ShipmentResponse->Response->ResponseStatus->Code == "1"){
		
		if($response_obj->ShipmentResponse->ShipmentResults->PackageResults->ShippingLabel->GraphicImage){

		@file_put_contents(PATH_LIB."ups/labels/".SHIP_LABEL,base64_decode($response_obj->ShipmentResponse->ShipmentResults->PackageResults->ShippingLabel->GraphicImage));
		
		$data = array(
					'status'			=> "CREATED",
					'shipping_account' 	=> $account,
					'label_url' 		=> SHIP_LABEL,
					//'label' 			=> $response_obj->ShipmentResponse->ShipmentResults->PackageResults->ShippingLabel->GraphicImage,
					'despacho_guia' 	=> $response_obj->ShipmentResponse->ShipmentResults->ShipmentIdentificationNumber,
					'shipment_cost'		=> $response_obj->ShipmentResponse->ShipmentResults->ShipmentCharges->TotalCharges->MonetaryValue,
					'fecha_label'		=> date('Y-m-d H:i:s')
					//'comments' 			=> "Documentado ".date('Y-m-d H:i:s')." via WS",
			);		
			
			$label_data = array(
				'client_id'	=> $client_id,
				'ship_id'	=> $shipment,
				'label_data' => $response_obj->ShipmentResponse->ShipmentResults->PackageResults->ShippingLabel->GraphicImage
			);
			$label_in = $db->insertAry("national_labels",$label_data);
			
			if($orderData['number']=="temp") $data['number'] = $data['despacho_guia'];


				if($create_type=="ecomm") { 
					$flgUp = $db->updateAry($clientData['db_table'],$data,"where ship_id = ".$shipment);
					if($reqtype=="prod") {
						$flgIn2 = $db->insertAry($db_trackinglog,array('referencia'=>$data['despacho_guia'],'shipping_service'=>$orderData['shipping_service'],'status'=>1,'client_id'=>$client_id));
					}
				} else if($create_type="ship"){
					$flgUp = $db->updateAry($_SESSION['LOGIN_ADMIN']['iso']."_shipping",$data,"where ship_id = ".$shipment);
					if($reqtype=="prod") $flgIn2 = $db->insertAry($db_trackinglog,array('referencia'=>$data['despacho_guia'],'shipping_service'=>$orderData['shipping_service'],'status'=>1,'store_id'=>$client_id));
					
				}

				if($flgUp){
					if($create_type=="ecomm"){
						$shipmentData = $db->getRowAssoc("select * from ".$clientData['db_table']." where ship_id = $shipment");
						sendShipmentEmail($create_type,$shipment,$clientData['client_id']);
					}
					else if($create_type="ship"){
						$shipmentData = $db->getRowAssoc("select * from ".$_SESSION['LOGIN_ADMIN']['iso']."_shipping where ship_id = $shipment");
						sendShipmentEmail($create_type,$shipment,$client_id);
					}	
			
				$output = array(
							"order_number" => $shipmentData['number'],
							"status" => $shipmentData['status'],
							"tracking" => $shipmentData['despacho_guia'],
						);
					
				}
		
		} else {
			$output = array("error"=>"Error courier. ".$response_obj->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode->Description);
			//$output .= print_r($response_obj);
		}
		
		 
	} else {
		$output = array("error"=>"Error de UPS generando envío: ".$response_obj->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode->Code." ".$response_obj->Fault->detail->Errors->ErrorDetail->PrimaryErrorCode->Description." ".$response_obj->Error->Description);
	}

	return json_encode($output);
	
}
?>
