<?php
	/*
	 * Carlos Servín carlos@servin.mx 
	 * SHOPIFY PHP API www.servin.mx
	 * 10-06-2018
	 */
	 
class Shopify_API{		
	
	 /*
	 public function __construct() {        
	 	$this->api_array();    
	 } 
	 */
	 public function console_log_debug($data){
	 	
  		echo '<script>';
  		echo 'console.log('. json_encode( $data ) .')';
  		echo '</script>';

	 }
	 
	 public function api_array(){
		 	global $db;
			$client_id = '21';			
		 	$api_array = $db->getRow("select * from national_shopify_apikeys where client_id = ".$client_id." and status = 1");
			$api_array['shopify_url']=str_replace('https://', '', $api_array['shopify_url']);
			return $api_array;			
	 }
	 
	 public function shopify_get($url){	 	
			$curl=curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_HTTPGET, 1); 		
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));			
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 	
			curl_setopt($curl, CURLOPT_HEADER, false);			 
			$output = curl_exec($curl); //execute and store server output
			curl_close($curl);
			$output=json_decode($output,true);			
			return $output;
	 }
	 
	 public function shopify_post($data, $url){	 	
		 	$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);	
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_VERBOSE, 0);
			curl_setopt($curl, CURLOPT_HEADER, 1);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
			$output = curl_exec ($curl);
			curl_close ($curl);			
			return json_encode($output);
	 }
	 
	 public function shopify_put($data, $url){
	 		$curl=curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);			
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));			
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($curl, CURLOPT_VERBOSE, 0);
			curl_setopt($curl, CURLOPT_HEADER, 1);			 
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");  
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);  
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
			$output = curl_exec($curl); //execute and store server output			
			curl_close($curl);			
			return json_encode($output);
	 }
	 
	 public function shopify_delete($data, $url){
		 	$curl=curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
			curl_setopt($curl, CURLOPT_HTTPGET, 1); 
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);  				
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));			
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 	
			curl_setopt($curl, CURLOPT_HEADER, false);			 
			$output = curl_exec($curl); 
			curl_close($curl);
			$output=json_decode($output,true);			
			return $output;
	 }
	 
	 public function id_shopify($id_order) {
		 	$api=$this->api_array();
			$url="https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
					  $api['shopify_url']."/admin/orders/".$id_order."/fulfillments.json";				
			$output=$this->shopify_get($url);				
			return $output;
	 }
	 //  Actualiza el shipping_address
	 public function sp_fullfillment_shipping_address($data, $id) {
			$api=$this->api_array();			
			$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				   $api['shopify_url']."/admin/orders/".$id.".json";	  
			$data_string = json_encode(array('order' => $data));	
			$output=$this-> shopify_put($data_string, $url);    			
			return $output;
	 }
	//  Revisar remover??
	 public function tracking_guide_shopify($data, $id,$id_shpf){
	 	$api=$this->api_array();
		
		$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				   $api['shopify_url']."/admin/orders/".$id_shpf.".json";
		
		$data_string = json_encode(array('order' => $data));
		
			$output=$this-> shopify_put($data_string, $url); 
			return $output;
	}
	 //  Revisar remover?? 
	 public function update_shopify_order2($data, $id, $id_shpf) {
	 		$api=$this->api_array();
				
			$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
					$api['shopify_url']."/admin/orders/".$id_shpf."/fulfillments/".$id.".json";
				
			$data_string = json_encode(array('fulfillment' => $data));	
			
			$output=$this-> shopify_put($data_string, $url);		
						
		return $output;
	 	            
	 } 
	 //  Revisar remover??	 
	 public function order_close($data,$id, $id_shpf ){
	 	$api=$this->api_array();
		
		$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
			$api['shopify_url']."/admin/orders/".$fullfillment."/fulfillments/".$fullfillment_id.".json";
			
		$data_string = json_encode(array('fulfillment' => $data));
		
		$output=$this-> shopify_put($data_string, $url);
		
		return $output;
		
	} 
	 // sp_locations método Verificado 
	 public function sp_locations($param){
		$api=$this->api_array();
				
		
		switch($param){
			case 'all_locations':	
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
						$api['shopify_url']."/admin/locations.json";		
				$output=$this->shopify_get($url);				
				return $output;				
				break;
			
			case 'single_location':
				$id_location='7619412027'; //Id exclusivo de MBE MX
			 	$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
						$api['shopify_url']."/admin/locations/".$id_location.".json";								
				$output=$this->shopify_get($url);	
				return $output;	
				break;
			
			case 'count_location':
				 $url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
						$api['shopify_url']."/admin/locations/count.json";							
				$output=$this->shopify_get($url);	
				return $output;	
			break;
			
			case 'list_all_inventory':
				$id_location='7619412027'; //Id exclusivo de MBE MX
			 	$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
						$api['shopify_url']."/admin/locations/".$id_location."/inventory_levels.json";
				//echo $url; 									
				$output=$this->shopify_get($url);	
				return $output;	
			break;
			
		}
		
	}

 	 //Shipping and fulfillment
	 public function shipping_fulfillment($param, $data, $order_id=null, $fulfillment_id=null, $event_id=null){
		$api=$this->api_array();			
		switch($param){
			case 'CreateCarrierService':
				//POST							
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/carrier_services.json";
				$data_string = json_encode(array('carrier_service' => $data));
				//Mandar un Post
				$output=$this-> shopify_post($data_string, $url);	
				return $output;
							
				break;
				
			case 'UpdateCarrierService':
				//PUT				
				$id_carrier_service='123456';				
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/carrier_services/".$id_carrier_service.".json";
				$data_string = json_encode(array('carrier_service' => $data));
				//Mandar un Put
				$output=$this-> shopify_put($data_string, $url);	
				return $output;				
				break;
			
			case 'RetrieveCarrierService': ///REVISAR 
				//PUT				
				$id_carrier_service='123456';				
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/carrier_services/".$id_carrier_service.".json";
				$data_string = json_encode(array('carrier_service' => $data));
				//Mandar un Put
				$output=$this-> shopify_put($data_string, $url);	
				return $output;				
				break;
			
			// Fulfillment Event
			
			case 'ReceiveFulfillmentEvent':				
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/orders/".$order_id."/fulfillments/".$fulfillment_id."/events.json";								
				$output=$this-> shopify_get($url);					
				return $output;
				break;
				
			case 'ReceiveSingleFulfillmentEvent':
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/orders/".$order_id."/fulfillments/".$fulfillment_id."/events/".$event_id.".json";
				$data_string = json_encode(array('carrier_service' => $data));
				//Mandar un Put
				$output=$this-> shopify_get($data_string, $url);	
				return $output;
				
			break;
				
			case 'CreateFulfillmentEvent':
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/orders/".$order_id."/fulfillments/".$fulfillment_id."/".$event_id.".json";
				$data_string = json_encode(array('fulfillment_events' => $data));
				//Mandar un Put
				$output=$this-> shopify_post($data_string, $url);	
				return $output;
			break;	
			
			case 'RemoveFulfillmentEvent':
				//pendiente construir método delete
				
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/orders/".$order_id."/fulfillments/".$fulfillment_id."/events".$event_id.".json";
				$data_string = json_encode(array('fulfillment_events' => $data));
				//Mandar un Put
				$output=$this-> shopify_delete($data_string, $url);	
				return $output;
			break;	
			
			case 'ReciveFulfillmentService':
				//GET /admin/fulfillment_services.json
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/orders/fulfillments_services.json";
				
				$data_string = json_encode(array('fulfillment_events' => $data));				
				$output=$this-> shopify_get($data_string, $url);	
				return $output;
			break;	
			
			case 'CreateFulfillmentService':
				// POST /admin/fulfillment_services.json
				
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/orders/fulfillments_services.json";				
				$data_string = json_encode(array('fulfillment_services' => $data));				
				$output=$this-> shopify_post($data_string, $url);	
				return $output;				
				break;
				
			case 'ReciveSingleFulfillmentService':
				//GET   /admin/fulfillment_services/#{fulfillment_service_id}.json
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/fulfillment_services/".$fulfillment_id.".json";
				
				$data_string = json_encode(array('fulfillment_services' => $data));				
				$output=$this-> shopify_get($data_string, $url);	
				return $output;				
				break;
			
			case 'ModifyFulfillmentService':
				// PUT /admin/fulfillment_services/#{fulfillment_service_id}.json
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/fulfillment_services/".$fulfillment_id.".json";
				
				$data_string = json_encode(array('fulfillment_services' => $data));				
				$output=$this-> shopify_put($data_string, $url);	
				return $output;	
				
			break;
				
			case 'RemoveFulfillmentService':				
				//DELETE /admin/fulfillment_services/#{fulfillment_service_id}.json
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/fulfillment_services/".$fulfillment_id.".json";
				
				$data_string = json_encode(array('fulfillment_services' => $data));				
				$output=$this-> shopify_delete($data_string, $url);	
				return $output;	
				break;			
			
			default:
			break;							
		}
		
	}
	
	 //
	 public function sp_fulfillment($param,$data=null, $order_id=null, $fulfillment_id=null, $event_id=null){
	 		$api=$this->api_array();
	/*$data=array('error'=>'"entra sp_fulfillment"',
						"param"=>$param,
						"data"=>$data,
						"order_id"=>$order_id,
						"fulfillment_id"=>$fulfillment_id,
						"event_id"=>$event_id,
						);
			$output.=$this->console_log_debug($data);*/
			switch($param){
				// GET /admin/orders/#{order_id}/fulfillments.json Receive a list of all Fulfillments
				case 'ReceiveFulfillments': //Verificada
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/orders/".$order_id."/fulfillments.json";							
				$output=$this-> shopify_get($url);	
				return $output;	
				break;
				
				case 'ReceiveCountFulfillments'://Verificada
				// GET /admin/orders/#{order_id}/fulfillments/count.json Receive a count of all Fulfillments
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/orders/".$order_id."/fulfillments/count.json";								
				$output=$this-> shopify_get($url);	
				return $output;	
				break;
				
				case 'ReceiveSingleFulfillment': //Verificada
				//GET /admin/orders/#{order_id}/fulfillments/#{fulfillment_id}.json Receive a single Fulfillment	
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/orders/".$order_id."/fulfillments/".$fulfillment_id.".json";											
				$output=$this-> shopify_get($url);	
				return $output;	
				break;
				
				case 'CreateFulfillments': //Verificada
				//POST /admin/orders/#{order_id}/fulfillments.json Create a new Fulfillment	
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/orders/".$order_id."/fulfillments.json";				
				$data_string = json_encode(array('fulfillment' => $data));								
				$output=$this-> shopify_post($data_string, $url);	
				return $output;	
				break;
				
				
				case 'ModifyFulfillments': //Verificada
				//PUT /admin/orders/#{order_id}/fulfillments/#{fulfillment_id}.json Modify an existing Fulfillment	
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/orders/".$order_id."/fulfillments/".$fulfillment_id.".json";				
				$data_string = json_encode(array('fulfillment' => $data));				
				$output=$this-> shopify_put($data_string, $url);
				$this->console_log_debug(array("data"=>$data_string, "url"=>$url));	
				return $output;	
				break;
				
				case 'CompleteFulfillments':
				//POST /admin/orders/#{order_id}/fulfillments/#{fulfillment_id}/complete.json Complete a fulfillment
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/orders/".$order_id."/fulfillments/".$fulfillment_id."/complete.json";
				
				$data_string = json_encode(array('fulfillments' => $data));				
				$output=$this-> shopify_post($data_string, $url);	
				return $output;	
				break;
				
				case 'TransitionFulfillments':
				//POST /admin/orders/#{order_id}/fulfillments/#{fulfillment_id}/open.json Transition a fulfillment from pending to open.	
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/orders/".$order_id."/fulfillments/".$fulfillment_id."/open.json";
				
				$data_string = json_encode(array('fulfillments' => $data));				
				$output=$this-> shopify_post($data_string, $url);	
				return $output;	
				break;
						
				case 'CancelFulfillments':		
				//POST /admin/orders/#{order_id}/fulfillments/#{fulfillment_id}/cancel.json
				$url = "https://".$api['shopify_apiuser'].":".$api['shopify_apipass']."@".
				$api['shopify_url']."/admin/orders/".$order_id."/fulfillments/".$fulfillment_id."cancel.json";
				
				$data_string = json_encode(array('fulfillments' => $data));				
				$output=$this-> shopify_delete($data_string, $url);	
				return $output;	
				break;
				
			
						
				default:
				break;
			}	
		
	
	 }
}

?>