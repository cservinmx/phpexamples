<?php

/*
 * Carlos Servín carlos@servin.mx 
 * NU SOAP examples www.servin.mx
 * 25-07-2018
 * Example 1
 * This webservice is the server
 * it has 2 methods called add & multiply, 
 * and does the operation of two numbers   
 *
 * */
 require('soap/nusoap.php');
 
function add($param1, $param2) {	
	return array('return'=>$param1+$param2);
}
 
function multiply($param1, $param2){	
	return array('return'=>$param1*$param2);
}
 
// Create the server instance
$server = new soap_server();
 
$ns = "http://localhost/phpexamples/SOAP_WS/example1_server";
 
// Initialize WSDL support
$server->configureWSDL('NAMEWDSL', $ns,'','document');
 
// Register the method to expose
$server->register('add',                // method name
    array('param1' => 'xsd:string',
		  'param2' => 'xsd:string'),    // input parameters
    array('return' => 'xsd:string'),    // output parameters
    $ns,         						// namespace
    "$ns#add",     						// soapaction
    'document',                         // style
    'literal',                          // use
    'Add Parameters'            		// documentation
);
 
// Register the method to expose
$server->register('multiply',               // method name
    array('param1' => 'xsd:string',
		  'param2' => 'xsd:string'	),    	// input parameters
    array('return' => 'xsd:string'),      	// output parameters
    $ns,             						// namespace
    "$ns#multiply",    						// soapaction
    'document',                             // style
    'literal',                            	// use
    'Multiply Parameters'            		// documentation
);
 
// Use the request to (try to) invoke the service
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
 ?>
