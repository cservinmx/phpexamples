<?php

/*
 * Carlos Servín carlos@servin.mx 
 * NU SOAP examples www.servin.mx
 * 25-07-2018
 * 
 * */
 require_once('soap/nusoap.php');
 
 function getprod($categoria){
 	if($categioria=="libros"){
 		return join(",", array( "book 1", "book 2", "book 3" )
					 );
 	}else{
 		return "No hay productos de esta categoría";
 	}
 }
 
  $server = new soap_server();
    $server->configureWSDL("producto", "urn:producto");
     
    $server->register("getProd",
        array("categoria" => "xsd:string"),
        array("return" => "xsd:string"),
        "urn:producto",
        "urn:producto#getProd",
        "rpc",
        "encoded",
        "Nos da una lista de productos de cada categoría");
      
    $server->service($HTTP_RAW_POST_DATA);

?>